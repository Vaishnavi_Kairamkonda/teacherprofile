package com.example.profile.ui.profile;

import com.google.android.material.appbar.MaterialToolbar;

/**
 * Created by elam on 17/08/21
 */
public interface ProfileCallback {

    void hideBottomNavigation();

    void showBottomNavigation();

    void setupActionBar(MaterialToolbar toolbar);
}
