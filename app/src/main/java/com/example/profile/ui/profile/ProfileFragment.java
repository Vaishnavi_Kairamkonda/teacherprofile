package com.example.profile.ui.profile;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.example.profile.R;
import com.example.profile.databinding.FragmentProfileBinding;
import com.example.profile.ui.support.SupportPhoneBottomSheetFragment;
import com.example.profile.utils.Common;
import com.example.profile.utils.Constants;
import com.example.profile.utils.SharedPrefHelper;

public class ProfileFragment extends Fragment {


    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    private ProfileViewModel profileViewModel;
    private FragmentProfileBinding binding;
    NavController navController;
    private Context mContext;
    private SharedPrefHelper mSharedPref;
    private ProfileCallback profileCallback;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        mSharedPref = SharedPrefHelper.getInstance(requireContext().getApplicationContext());
    }
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentProfileBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);

        binding.tvLogout.setOnClickListener(v ->
                navController.navigate(R.id.action_navigation_profile_to_fragment_Teacher_Account));

        if(profileCallback != null){
            profileCallback.showBottomNavigation();
        }

        PreferenceFragment preferenceFragment = new PreferenceFragment();
        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.flPreferenceScreen, preferenceFragment)
                .commit();

        preferenceFragment.setCallback(new PreferenceFragment.Callback() {
            @Override
            public void openAccount() {
//                profileCallback.hideBottomNavigation();


                NavDirections toTeacherAccountFragment = ProfileFragmentDirections.actionNavigationProfileToFragmentTeacherAccount();
                Navigation.findNavController(binding.getRoot()).navigate(toTeacherAccountFragment);
            }

            @Override
            public void shareAppLink() {

            }

            @Override
            public void rateUs() {
                final String appPackageName = requireActivity().getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }

            @Override
            public void supportEmail(String email) {
                Uri uri = Uri.parse("mailto:" + email);
                Common.openSupportedApp(uri, requireContext(), getString(R.string.email));

            }

            @Override
            public void supportMobile(String mobileNumber) {
                SupportPhoneBottomSheetFragment supportPhoneBottomSheetFragment = new SupportPhoneBottomSheetFragment();
                Bundle bundle = new Bundle();
                bundle.putString(MOBILE_NUMBER,mobileNumber);
                supportPhoneBottomSheetFragment.setArguments(bundle);
                FragmentManager childFragmentManager = getChildFragmentManager();
                Fragment oldFragment = childFragmentManager.findFragmentByTag(Constants.SUPPORT_BOTTOM_SHEET_DIALOG_TAG);
                if (oldFragment != null) {
                    childFragmentManager.beginTransaction().remove(oldFragment).commit();
                }
                childFragmentManager.beginTransaction().add(supportPhoneBottomSheetFragment, Constants.SUPPORT_BOTTOM_SHEET_DIALOG_TAG);
                supportPhoneBottomSheetFragment.show(childFragmentManager, Constants.SUPPORT_BOTTOM_SHEET_DIALOG_TAG);

            }
        } );



        if(Common.isObjectNotNullOrEmpty(mSharedPref.getUserEmail())) {
            binding.tvLoggedInEmail.setText("logged in as\n" + mSharedPref.getUserEmail());
        }
        binding.tvLogout.setOnClickListener(v -> {

            new AlertDialog.Builder(requireContext())
                    .setMessage(requireContext().getString(R.string.logout_confirmation))
                    .setPositiveButton(R.string.profile_logout, (dialog, which) -> {
                       // profileViewModel.clearAllTables();
                        Common.tokenMismatchHandle(requireActivity());
                    })
                    .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
                    .setCancelable(true)
                    .show();
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}