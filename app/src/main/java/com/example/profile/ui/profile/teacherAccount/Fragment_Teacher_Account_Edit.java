package com.example.profile.ui.profile.teacherAccount;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.profile.R;

import com.example.profile.databinding.FragmentTeacherAccountEditBinding;
import com.google.android.material.textfield.TextInputLayout;


public class Fragment_Teacher_Account_Edit extends Fragment {

    CheckBox checkBox;
    AutoCompleteTextView ListOfSubjects;
    TextInputLayout fullName, emailAddress, phoneNumber,subjectList, contactEmailAddress;
    FragmentTeacherAccountEditBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //To get in toolbar
        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle("Update Account");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment__teacher__account__edit, container, false);
        binding = FragmentTeacherAccountEditBinding.bind(v);
        binding.getRoot();
        checkBox = binding.checkbox;
        fullName = binding.tilteacherFullName;
        emailAddress = binding.tilEmailAddress;
        phoneNumber = binding.tilteacherPhoneNumber;
        subjectList = binding.tilSubjects;
        ListOfSubjects = binding.tvSubjects;

        contactEmailAddress = binding.tilContactEmailAddress;


        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        subjectList.setOnClickListener(v -> {
            ListOfSubjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    final String[] items = {"Maths", "Physics", "Chemistry",};

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Select Subjects")

                            .setItems(items, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getContext(), items[which] + " is selected", Toast.LENGTH_SHORT).show();
                                }
                            });
                    builder.setPositiveButton("OK", null);
                    builder.setNegativeButton("CANCEL", null);
                    AlertDialog alertDialog = builder.create();

                    alertDialog.show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    contactEmailAddress.setVisibility(View.INVISIBLE);

                } else {
                    contactEmailAddress.setVisibility(View.VISIBLE);

                }

            }
        });

    }
}