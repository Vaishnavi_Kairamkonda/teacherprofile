package com.example.profile.ui.profile;

import android.os.Bundle;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.profile.BuildConfig;
import com.example.profile.R;
import com.example.profile.utils.Constants;


/**
 * Created by elam on 17/08/21
 */
public class PreferenceFragment extends PreferenceFragmentCompat {


    private Callback callback;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.profile_preference, rootKey);

        Preference appVersion = findPreference(getString(R.string.preference_key_app_version));
        Preference account = findPreference(getString(R.string.preference_key_account));

        if (account != null) {
            account.setOnPreferenceClickListener(preference ->{
                callback.openAccount();
                return true;
            });
        }

        Preference rateUs = findPreference(getString(R.string.preference_key_rate_us));
        if (rateUs != null) {
            rateUs.setOnPreferenceClickListener(preference ->{
                callback.rateUs();
                return true;
            });
        }

        Preference supportEmail = findPreference(getString(R.string.preference_key_support_email));
        if (supportEmail != null) {
            supportEmail.setOnPreferenceClickListener(preference ->{
               callback.supportEmail(supportEmail.getSummary().toString());
                return true;
            });
        }

        Preference supportMobile = findPreference(getString(R.string.preference_key_support_mobile));
        if (supportMobile != null) {
            supportMobile.setOnPreferenceClickListener(preference ->{
               callback.supportMobile(supportMobile.getSummary().toString());
                return true;
            });
        }

            assert appVersion != null;
            appVersion.setSummary(appVersionName());

    }

    public String appVersionName() {
        String appVersionName = BuildConfig.VERSION_NAME;

        if(Constants.DEVELOPMENT_MODE){
           return getString(R.string.development_mode) + appVersionName;
        } else {
            return appVersionName;
        }
    }


    public interface Callback{

        void openAccount();

        void shareAppLink();

        void rateUs();

        void supportEmail(String email);

        void supportMobile(String mobileNumber);
    }

    public Callback getCallback() {
        return callback;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }
}
