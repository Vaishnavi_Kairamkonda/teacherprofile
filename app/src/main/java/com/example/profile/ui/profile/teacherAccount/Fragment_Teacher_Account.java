package com.example.profile.ui.profile.teacherAccount;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.profile.R;
import com.example.profile.databinding.FragmentTeacherAccountBinding;


public class Fragment_Teacher_Account extends Fragment {

    FragmentTeacherAccountBinding binding;
    NavController navController;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
       // getActivity().setTitle("Account");
        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle("Account");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment__teacher__account, container, false);
        binding = FragmentTeacherAccountBinding.bind(v);

        return v;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);

        //binding..setOnClickListener(v ->
          //      navController.navigate(R.id.action_navigation_profile_to_fragment_Teacher_Account));

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // First clear current all the menu items
        menu.clear();

        // Add the new menu items
        inflater.inflate(R.menu.edit_data, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_account_edit:
                navController.navigate(R.id.action_fragment_Teacher_Account_to_fragment_Teacher_Account_Edit);
                //Log.d( getContext(),"Will post the photo to server");
                return true;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}