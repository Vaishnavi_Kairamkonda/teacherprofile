package com.example.profile.ui.support;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.example.profile.R;
import com.example.profile.databinding.SupportPhoneBottomSheetBinding;
import com.example.profile.ui.profile.ProfileFragment;
import com.example.profile.utils.Common;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

/**
 * Created by elam on 03/09/21
 */
public class SupportPhoneBottomSheetFragment extends BottomSheetDialogFragment {

    private SupportPhoneBottomSheetBinding mBinding;
    private String mobileNumber;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if(arguments != null){
             mobileNumber = arguments.getString(ProfileFragment.MOBILE_NUMBER);
        }
        else{
            mobileNumber = getString(R.string.contact_us2);
        }
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        mBinding = SupportPhoneBottomSheetBinding.inflate(getLayoutInflater(),container,false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.phone.getRoot().setOnClickListener(this::openDialer);
        mBinding.message.getRoot().setOnClickListener(this::openMessage);

        mBinding.tvMobileNumber.setText(mobileNumber);
       mBinding.phone.ivIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_baseline_local_phone_24,null));
        mBinding.phone.tvLabel.setText(getString(R.string.phone));

        mBinding.message.ivIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_baseline_message_24,null));
        mBinding.message.tvLabel.setText(getString(R.string.message));


    }

    private void openMessage(View view) {
        Uri uri = Uri.parse("sms:" + mobileNumber);
        Common.openSupportedApp(uri, requireContext(),getString(R.string.message));
    }

    private void openDialer(View view) {
        Uri uri = Uri.parse("tel:" + mobileNumber);
        Common.openSupportedApp(uri, requireContext(),getString(R.string.phone));
    }

}
