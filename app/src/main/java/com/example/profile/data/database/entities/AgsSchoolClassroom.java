package com.example.profile.data.database.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Used in:
 *      LoginRepository(getLoginAndRegistrationApis().login)
 *      SchoolRepository(getSchoolApis().addClassroom, getSchoolApis().updateClassroom)
 */

@Entity(tableName = "AGS_SCHOOL_CLASSROOM",foreignKeys = {@ForeignKey(entity = AgsSchool.class,parentColumns = "school_id", childColumns = "school_id",onDelete = ForeignKey.CASCADE)})
public class AgsSchoolClassroom {

    @PrimaryKey
    @ColumnInfo(name = "class_id")
    @SerializedName("class_id")
    private int classId;

    @ColumnInfo(name = "school_id",  index = true)
    private int schoolId;

    @ColumnInfo(name = "class")
    @SerializedName("class")
    private int standard;

    @ColumnInfo(name = "class_name")
    @SerializedName("class_name")
    private String className;

    @ColumnInfo(name = "student_join_code")
    @SerializedName("student_join_code")
    private String studentJoinCode;

    @Ignore
    @SerializedName("is_deleted")
    private int isDeleted;

    private long lastUpdated;
    private int status;

    private String message;

    public AgsSchoolClassroom(int status, String message, int classId, int schoolId, int standard, String className, String studentJoinCode, long lastUpdated) {
        this.status = status;
        this.message = message;
        this.classId = classId;
        this.schoolId = schoolId;
        this.standard = standard;
        this.className = className;
        this.studentJoinCode = studentJoinCode;
        this.lastUpdated = lastUpdated;
    }
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public int getStandard() {
        return standard;
    }

    public void setStandard(int standard) {
        this.standard = standard;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getStudentJoinCode() {
        return studentJoinCode;
    }

    public void setStudentJoinCode(String studentJoinCode) {
        this.studentJoinCode = studentJoinCode;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        AgsSchoolClassroom agsClassRoom = (AgsSchoolClassroom) obj;
        return agsClassRoom.getClassId() == this.getClassId() && agsClassRoom.getClassName().equals(this.getClassName()) && agsClassRoom.getSchoolId() == this.getSchoolId() && agsClassRoom.getStandard() == this.getStandard() && agsClassRoom.getStudentJoinCode().equals(this.getStudentJoinCode());
    }
}
