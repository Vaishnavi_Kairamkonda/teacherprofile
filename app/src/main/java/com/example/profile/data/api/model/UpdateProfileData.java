package com.example.profile.data.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by elam on 20/08/21
 */
public class UpdateProfileData {


    @SerializedName("app_version")
    private int appVersion;

    @SerializedName("profile_type")
    private int profileType;

    private Teacher teacher;

    public UpdateProfileData(int appVersion, int profileType, Teacher teacher) {
        this.appVersion = appVersion;
        this.profileType = profileType;
        this.teacher = teacher;
    }

    public int getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(int appVersion) {
        this.appVersion = appVersion;
    }

    public int getProfileType() {
        return profileType;
    }

    public void setProfileType(int profileType) {
        this.profileType = profileType;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
