package com.example.profile.data.database.entities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Objects;

/**
 * Used in:
 *      LoginRepository(getLoginAndRegistrationApis().login)
 *      RegistrationRepository(getLoginAndRegistrationApis().verifyProfile)
 */
@Entity(tableName = "AGS_SCHOOL")
public class AgsSchool {

    @PrimaryKey
    @ColumnInfo(name = "school_id")
    private int id;

    @ColumnInfo(name = "school_name")
    private String name;

    @ColumnInfo(name = "school_teacher_code")
    private String teacherCode;

    @ColumnInfo(name = "contact_person")
    private String contactPerson;

    @ColumnInfo(name = "contact_number")
    private String contactNumber;

    @ColumnInfo(name = "school_email")
    private String email;

    private String state;

    private long teacherLastUpdated;

    public AgsSchool(int id, String name, String teacherCode, String contactPerson, String contactNumber, String email, String state, long teacherLastUpdated) {
        this.id = id;
        this.name = name;
        this.teacherCode = teacherCode;
        this.contactPerson = contactPerson;
        this.contactNumber = contactNumber;
        this.email = email;
        this.state = state;
        this.teacherLastUpdated = teacherLastUpdated;
    }

    @Ignore
    public AgsSchool(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacherCode() {
        return teacherCode;
    }

    public void setTeacherCode(String teacherCode) {
        this.teacherCode = teacherCode;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getTeacherLastUpdated() {
        return teacherLastUpdated;
    }

    public void setTeacherLastUpdated(long teacherLastUpdated) {
        this.teacherLastUpdated = teacherLastUpdated;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        AgsSchool school = (AgsSchool) obj;
        return Objects.equals(school.getName(), this.getName()) &&
                Objects.equals(school.getContactPerson(), this.getContactPerson()) &&
        Objects.equals(school.getState(), this.getState()) &&
                Objects.equals(school.getContactNumber(), this.getContactNumber()) &&
                Objects.equals(school.getEmail(), this.getEmail());
    }
}
