package com.example.profile.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by elam on 12/10/21
 */
public class Teacher {

    @SerializedName("teacher_id")
    private int id;

    private String name;

    private String phone;

    private String email;

    @SerializedName("communication_email")
    private String communicationEmail;

    @SerializedName("subjects")
    private List<String> subjectList;

    @SerializedName("teacher_status")
    private String teacherStatus;

    private School school;

    public Teacher(int id, String name, String phone, String email, String communicationEmail, List<String> subjectList, String teacherStatus) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.communicationEmail = communicationEmail;
        this.subjectList = subjectList;
        this.teacherStatus = teacherStatus;
    }

    public Teacher(String name, String phone, String email, String communicationEmail, List<String> subjectList) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.communicationEmail = communicationEmail;
        this.subjectList = subjectList;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCommunicationEmail() {
        return communicationEmail;
    }

    public void setCommunicationEmail(String communicationEmail) {
        this.communicationEmail = communicationEmail;
    }

    public List<String> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<String> subjectList) {
        this.subjectList = subjectList;
    }

    public String getTeacherStatus() {
        return teacherStatus;
    }

    public void setTeacherStatus(String teacherStatus) {
        this.teacherStatus = teacherStatus;
    }
}
