package com.example.profile.data.api.errorHandling;

/**
 * Created by elam on 23/10/21
 */
public class TeacherCancelRequestApiStatusResponse {

    private int status;
    private String message;
    private String teacherStatus;

    public TeacherCancelRequestApiStatusResponse(int status, String message, String teacherStatus) {
        this.status = status;
        this.message = message;
        this.teacherStatus = teacherStatus;
    }

    public String getTeacherStatus() {
        return teacherStatus;
    }

    public void setTeacherStatus(String teacherStatus) {
        this.teacherStatus = teacherStatus;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
