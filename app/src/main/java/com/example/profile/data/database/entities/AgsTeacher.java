package com.example.profile.data.database.entities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "AGS_TEACHER")
public class AgsTeacher {
    @PrimaryKey
    private int id;

    private String name;

    private String phoneNumber;

    private String email;

    private String communicationEmail;

    private String subject;

    private String teacherStatus;

    public AgsTeacher(int id, String name, String phoneNumber, String email, String communicationEmail, String subject, String teacherStatus) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.communicationEmail = communicationEmail;
        this.phoneNumber = phoneNumber;
        this.subject = subject;
        this.teacherStatus = teacherStatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getSubject() {
        return subject;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeacherStatus() {
        return teacherStatus;
    }

    public void setTeacherStatus(String requestStatus) {
        this.teacherStatus = requestStatus;
    }

    public int getId(int currentProfileId) {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }

    public String getCommunicationEmail() {
        return communicationEmail;
    }

    public void setCommunicationEmail(String communicationEmail) {
        this.communicationEmail = communicationEmail;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass())
            return false;
        if (obj == this) return true;
        AgsTeacher agsTeacher = (AgsTeacher) obj;
        return Objects.equals(agsTeacher.getName(), this.name) && Objects.equals(agsTeacher.getEmail(), this.getEmail()) && Objects.equals(agsTeacher.getCommunicationEmail(), this.getCommunicationEmail()) &&
                Objects.equals(agsTeacher.getPhoneNumber(), this.getPhoneNumber()) && Objects.equals(agsTeacher.getTeacherStatus(), this.getTeacherStatus()) && Objects.equals(agsTeacher.getSubject(), this.getSubject());
    }

}

