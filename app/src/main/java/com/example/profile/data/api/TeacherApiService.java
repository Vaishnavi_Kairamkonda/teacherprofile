package com.example.profile.data.api;



import com.example.profile.data.api.model.TeacherRegistrationSubjectResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by elam on 12/10/21
 */
public interface TeacherApiService {



    @GET("getTeacherRegistrationSubject.json")
    Call<TeacherRegistrationSubjectResponse> getTeacherRegistrationSubjects(@Header("Authorization") String token);


}
