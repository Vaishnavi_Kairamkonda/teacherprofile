package com.example.profile.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TeacherRegistrationSubjectData {

    @SerializedName("teacher_registration_subject")
    private List<String> subjectsList;

    public TeacherRegistrationSubjectData(List<String> subjectsList) {
        this.subjectsList = subjectsList;
    }

    public List<String> getSubjectsList() {
        return subjectsList;
    }

    public void setSubjectsList(List<String> subjectsList) {
        this.subjectsList = subjectsList;
    }
}
