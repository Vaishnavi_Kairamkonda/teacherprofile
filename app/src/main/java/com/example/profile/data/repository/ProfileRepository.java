package com.example.profile.data.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;


import com.example.profile.R;
import com.example.profile.data.api.RetrofitClient;
import com.example.profile.data.api.errorHandling.ApiStatusResponse;
import com.example.profile.data.api.errorHandling.Resource;
import com.example.profile.data.api.model.Teacher;
import com.example.profile.data.api.model.TeacherRequestStatus;
import com.example.profile.data.api.model.UpdateProfileData;
import com.example.profile.data.api.model.UpdateProfileResponse;
import com.example.profile.data.database.AgsDatabase;
import com.example.profile.data.database.dao.AgsTeachersDao;
import com.example.profile.data.database.entities.AgsTeacher;
import com.example.profile.utils.Common;
import com.example.profile.utils.Constants;
import com.example.profile.utils.SharedPrefHelper;

import org.jetbrains.annotations.NotNull;

import java.net.SocketTimeoutException;
import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elam on 09/08/21
 */
public class ProfileRepository {

    private final SharedPrefHelper mSharedPref;
    private final AgsDatabase agsDatabase;
    protected Context applicationContext;
    private AgsTeachersDao agsTeachersDao;
    AgsTeacher agsTeacher;



    public ProfileRepository(Context applicationContext, AgsDatabase agsDatabase) {
        this.applicationContext = applicationContext;
        mSharedPref = SharedPrefHelper.getInstance(applicationContext);
        this.agsDatabase = agsDatabase;

        this.agsTeachersDao  = agsDatabase.agsTeachersDao();

    }


    public void clearAllTables(){
        AgsDatabase.databaseWriteExecutor.execute(agsDatabase::clearAllTables);
    }

   /* public LiveData<AgsTeacher> getStudentProfile(){
        return Transformations.map(agsTeacher.getId(mSharedPref.getCurrentProfileId()), agsTeacher -> {
            if(agsTeacher == null){
                return null;
            }
            AgsTeacher agsTeacher1 = new AgsTeacher(agsTeacher1.getSchool(), agsTeacher1.getName(), agsTeacher1.getEmail(), "", agsTeacher1.getCommunicationEmail());
            agsTeacher1.setSubject(agsTeacher1.getSubject());
            agsTeacher1.setPhoneNumber(agsTeacher1.getPhoneNumber());
            return agsTeacher1;
        });
    }

    */






    public LiveData<Resource<ApiStatusResponse>> updateProfile(UpdateProfileData updateProfileData) {
        MutableLiveData<Resource<ApiStatusResponse>> apiResponse = new MutableLiveData<>();

        if (!Common.isInternetConnected(applicationContext)) {
            apiResponse.postValue(Resource.error(applicationContext.getResources().getString(R.string.connection_error_msg), new ApiStatusResponse(Constants.STATUS_CODE_CONNECTIVITY_ISSUE, applicationContext.getResources().getString(R.string.connection_error_msg))));
            return apiResponse;
        }

        Call<UpdateProfileResponse> updateProfileResponseCall = RetrofitClient.getInstance(applicationContext).getCommonApiService().updateProfile(Constants.BEARER_TOKEN_PREFIX + mSharedPref.getUserToken(), mSharedPref.getCurrentProfileId(), mSharedPref.getProfileType(), updateProfileData);

        updateProfileResponseCall.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(@NotNull Call<UpdateProfileResponse> call, @NotNull Response<UpdateProfileResponse> response) {

                UpdateProfileResponse body = response.body();
                if (body == null) {
                    apiResponse.postValue(Resource.error("", new ApiStatusResponse(response.code(), response.message())));
                    return;
                }

                if(body.getStatus() == Constants.STATUS_CODE_SUCCESS){
                    if(updateProfileData.getAppVersion() == Constants.IGNORE_APP_VERSION){
                        if(updateProfileData.getProfileType() == Constants.PROFILE_TYPE_STUDENT && updateProfileData.getTeacher() != null) {
                            updateStudentProfile(updateProfileData.getTeacher().getId(), updateProfileData.getTeacher().getName(), updateProfileData.getTeacher().getPhone(), updateProfileData.getTeacher().getEmail(), updateProfileData.getTeacher().getCommunicationEmail(), updateProfileData.getTeacher().getSubjectList(), updateProfileData.getTeacher().getTeacherStatus());
                        }
                    }
                    else {
                        mSharedPref.setAppVersionCode(updateProfileData.getAppVersion());
                    }
                    apiResponse.postValue(Resource.success(new ApiStatusResponse(body.getStatus(), body.getMessage())));
                }
                else if (body.getStatus() == Constants.STATUS_CODE_TOKEN_MISMATCH) {
                    clearAllTables();
                    apiResponse.postValue(Resource.error("", new ApiStatusResponse(body.getStatus(), body.getMessage())));
                }
                else {
                    apiResponse.postValue(Resource.error("", new ApiStatusResponse(body.getStatus(), body.getMessage())));
                }
            }

            private void updateStudentProfile(int id, String name, String phone, String email, String communicationEmail, List<String> subjectList, String teacherStatus) {
            }

            @Override
            public void onFailure(@NotNull Call<UpdateProfileResponse> call, @NotNull Throwable t) {
                if (t instanceof RetrofitClient.NoConnectivityException) {
                    // No internet connection
                    apiResponse.postValue(Resource.error("", new ApiStatusResponse(Constants.STATUS_CODE_CONNECTIVITY_ISSUE, applicationContext.getResources().getString(R.string.connection_error_msg))));
                } else if (t instanceof SocketTimeoutException) {
                    apiResponse.postValue(Resource.error("", new ApiStatusResponse(Constants.STATUS_CODE_TIMEOUT, applicationContext.getResources().getString(R.string.connection_timeout_msg))));
                } else {
                    apiResponse.postValue(Resource.error("", new ApiStatusResponse(-1, t.getMessage())));
                }
            }
        });

        return apiResponse;

    }

    private void updateStudentProfile(List<TeacherRequestStatus> teacherRequestStatusesList) {
        AgsDatabase.databaseWriteExecutor.execute(() -> agsTeachersDao.updateTeachersStatus(teacherRequestStatusesList));

    }


}
