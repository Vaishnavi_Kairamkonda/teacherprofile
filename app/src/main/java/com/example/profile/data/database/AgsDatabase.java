package com.example.profile.data.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;


import com.example.profile.data.database.dao.AgsTeachersDao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {AgsTeachersDao.class},version = 2)
public abstract class AgsDatabase extends RoomDatabase {

    public abstract AgsTeachersDao agsTeachersDao();



    public static final ExecutorService databaseWriteExecutor =
            Executors.newSingleThreadExecutor();

    @Override
    public void clearAllTables() {

    }
}
