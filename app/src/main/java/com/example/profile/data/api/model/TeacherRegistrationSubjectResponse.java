package com.example.profile.data.api.model;

import com.google.gson.Gson;

public class TeacherRegistrationSubjectResponse {

    private int status;
    private String message;
    private TeacherRegistrationSubjectData data;

    public TeacherRegistrationSubjectResponse(int status, String message, TeacherRegistrationSubjectData data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TeacherRegistrationSubjectData getData() {
        return data;
    }

    public void setData(TeacherRegistrationSubjectData data) {
        this.data = data;
    }

    public static TeacherRegistrationSubjectResponse fromJSON(String jsonString) {
        return new Gson().fromJson(jsonString, TeacherRegistrationSubjectResponse.class);
    }
}
