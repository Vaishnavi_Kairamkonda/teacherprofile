package com.example.profile.data.api.model;

import com.example.profile.data.database.entities.AgsSchoolClassroom;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class School {

    @SerializedName("school_id")
    private int id;

    @SerializedName("school_master_id")
    private int schoolMasterId;

    @SerializedName("school_name")
    private String name;

    @SerializedName("contact_person")
    private String contactPerson;

    @SerializedName("contact_number")
    private String contactNumber;

    @SerializedName("school_email")
    private String schoolEmail;

    private String state;

//    private String city;

    @SerializedName("school_teacher_code")
    private String schoolTeacherCode;

    @SerializedName("app_user_id")
    private String appUserId;

    @SerializedName("class")
    private List<AgsSchoolClassroom> classRoomList;

    public School(String name, String contactPerson, String contactNumber, String schoolEmail,
                  /*String city,*/ String state) {
        this.name = name;
        this.contactPerson = contactPerson;
        this.contactNumber = contactNumber;
        this.schoolEmail = schoolEmail;
//        this.city = city;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSchoolTeacherCode() {
        return schoolTeacherCode;
    }

    public void setSchoolTeacherCode(String schoolTeacherCode) {
        this.schoolTeacherCode = schoolTeacherCode;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static School fromJson(String jsonString) {
        return new Gson().fromJson(jsonString, School.class);
    }

    public String getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(String appUserId) {
        this.appUserId = appUserId;
    }

    public List<AgsSchoolClassroom> getClassRoomList() {
        return classRoomList;
    }

    public void setClassRoomList(List<AgsSchoolClassroom> classRoomList) {
        this.classRoomList = classRoomList;
    }

    public int getSchoolMasterId() {
        return schoolMasterId;
    }

    public void setSchoolMasterId(int schoolMasterId) {
        this.schoolMasterId = schoolMasterId;
    }
}
