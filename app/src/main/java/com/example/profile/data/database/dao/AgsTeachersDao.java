package com.example.profile.data.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;


import com.example.profile.data.api.model.TeacherRequestStatus;

import com.example.profile.data.database.entities.AgsTeacher;
import com.example.profile.utils.Constants;

import java.util.List;

@Dao
abstract public class AgsTeachersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(AgsTeacher... teachers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(List<AgsTeacher> teachersList);

    @Update
    public abstract void update(AgsTeacher teachers);

    @Query("SELECT * FROM AGS_TEACHER where teacherStatus=:teacherStatus")
    public abstract LiveData<List<AgsTeacher>> getTeachersByStatus( String teacherStatus);

    @Query("SELECT * FROM AGS_TEACHER where teacherStatus=:statusApproved OR teacherStatus=:statusSuspended")
    public abstract LiveData<List<AgsTeacher>> getApprovedOrSuspendedTeachers( String statusApproved, String statusSuspended);

    @Query("SELECT COUNT(*) FROM AGS_TEACHER where teacherStatus=:teacherStatus")
    public abstract LiveData<Integer> getTeachersRequestCount(String teacherStatus);

    @Query("UPDATE AGS_TEACHER SET teacherStatus=:requestStatus WHERE id =:id")
    public abstract void updateTeacherStatus(int id, String requestStatus);

    @Query("DELETE FROM AGS_TEACHER WHERE id=:id")
    public abstract void deleteTeacher(int id);

    @Transaction
    public void updateTeachersStatus(List<TeacherRequestStatus> teacherRequestStatusesList){
        for(TeacherRequestStatus teacherRequestStatus : teacherRequestStatusesList){
            if(teacherRequestStatus.getRequestStatus().equals(Constants.TEACHER_STATUS_DELETED))
                deleteTeacher(teacherRequestStatus.getId());
            else
            updateTeacherStatus(teacherRequestStatus.getId(),teacherRequestStatus.getRequestStatus());
        }
    }

    @Query("SELECT teacherStatus FROM AGS_TEACHER WHERE id =:currentProfileId")
    public abstract LiveData<String> getTeacherStatusById(int currentProfileId);

}