package com.example.profile.data.api.model;

/**
 * Created by elam on 19/08/21
 */
public class UpdateProfileResponse {

    private int status;

    private String message;


    public UpdateProfileResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
