package com.example.profile.data.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by elam on 17/09/21
 */
public class TeacherRequestStatus {

    @SerializedName("teacher_id")
    private int id;
    @SerializedName("teacher_status")
    private String requestStatus;

    public TeacherRequestStatus(int id, String requestStatus) {
        this.id = id;
        this.requestStatus = requestStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }
}
