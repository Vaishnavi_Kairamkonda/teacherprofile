package com.example.profile.data.api;



import com.example.profile.data.api.model.UpdateProfileData;
import com.example.profile.data.api.model.UpdateProfileResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by elam on 20/08/21
 */
public interface CommonApiService {


    @POST("updateProfile.json")
    Call<UpdateProfileResponse> updateProfile(@Header("Authorization") String token, @Query("profile_id") int profileId, @Query("profile_type") int profileType, @Body UpdateProfileData updateProfileData);

  //  @GET("getTestByIdForProfile.json")
   // Call<TestResponse> getTestByIdForProfile(@Header("Authorization") String token, @Query("test_id") int testId, @Query("profile_id") int profileId, @Query("profile_type") int profileType, @Query("subject_id") int subjectId, @Query("chapter_id") int chapterId, @Query("assignment_id") int assignmentId);

}
