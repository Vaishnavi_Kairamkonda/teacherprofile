package com.example.profile.utils;

import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static final String DEBUG_TAG = "AGSchools";

    //*********** Production ***************//
//    public static final boolean DEVELOPMENT_MODE = false;
    //*********** Production ***************//

    //*********** Development ***************//
    public static final boolean DEVELOPMENT_MODE = true;
    //*********** Development ***************//

    public static final String AHA_TNC_URL = "https://www.ahaguru.com/tncs";
    public static final String MIXPANEL_TOKEN ="f05d3b5db623366c5556e94ff8bc46e0";

    // Bundle Key Constants
    public static final String WEB_VIEW_URL_EXTRA = "terms_and_conditions_url";

    //Login
    public static final int REQUEST_CODE_SIGN_IN = 101;

    // Hardcoding Class 10
    public static final int STANDARD_10 = 10;

    //Profile type
    public static final int PROFILE_TYPE_NOT_YET_LINKED = 0;
    public static final int PROFILE_TYPE_SCHOOL = 1;
    public static final int PROFILE_TYPE_TEACHER = 2;
    public static final int PROFILE_TYPE_STUDENT = 3;

    public static final int MOBILE_NUMBER_MIN_LENGTH = 6;
    public static final int MOBILE_NUMBER_MAX_LENGTH = 10;
    public static final String AHA_GURU_STAGING_SERVER = "http://3.110.3.169/Agschool/";
    public static final String AHA_GURU_PRODUCTION_SERVER = "https://school.ahaguru.com/Agschool/";


    // API Response Codes
    public static final int STATUS_CODE_SUCCESS = 200;
    public static final int STATUS_CODE_TOKEN_MISMATCH = 401;
    public static final int STATUS_CODE_CONNECTIVITY_ISSUE = 504;
    public static final int STATUS_CODE_RESPONSE_NOT_FOUND = 404;
    public static final int INVALID_COURSE_TYPE = 400;
    public static final int DATA_NOT_FOUND = 404;
    public static final int STATUS_CODE_TIMEOUT = 408;
    public static final int STATUS_ERROR_CODE_CONFLICT = 409;
    public static final int STATUS_CODE_INVALID_USER = 412;

    public static final String MOBILE_NO_DELIMITER = "-";
    public static final String BEARER_TOKEN_PREFIX = "Bearer ";
    public static final int DEFAULT_COUNTRY_CODE = 91;
    public static final int DEFAULT_LEAD_ID = -1;

    public static final List<String> standardList = new ArrayList<String>(){{
       add("KG");
        add("1");
        add("2");
        add("3");
        add("4");
        add("5");
        add("6");
        add("7");
        add("8");
        add("9");
        add("10");
        add("11");
        add("12");
        add("12+");
    }};


    //intent extra strings
    public static final String SUBJECT_ID = "subject_id";
    public static final String CHAPTER_ID = "chapter_id";
    public static final String TEST_ID = "test_id";
    public static final String MAPPING_ID = "mapping_id";
    public static final String ASSIGNMENT_ID = "assignment_id";
    public static final String TEST_COMPLETION_STATUS = "test_completion_status";
    public static final String TEST_TIME_TAKEN = "time_taken";
    public static final String TEST_TOTAL_SCORE = "test_score";
    public static final String TEST_NAME = "test_name";
    public static final String ASSIGNMENT_NAME = "assignment_name";
    public static final String SCHOOL_NAME = "school_name";

    public static final String ELEMENT_TYPE = "elementtype";
    public static final String ELEMENT_TEST_DURATION = "elementtestduration";
    public static final String ELEMENT_COMPLETION_LEVEL = "elementcompletionlevel";

    public static final String APP_PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=com.ahaguru.schools";
    public static final String REGEX_WITH_TEXT_NUMBER_SPACE = "[^a-zA-Z 0-9]*";
    public static final String REGEX_WITH_TEXT_NUMBER = "[^a-zA-Z0-9]*";
    public static final String REGEX_WITH_TEXT_SPACE_DOT = "[^a-zA-Z .]*";

    public static final int IGNORE_APP_VERSION = -1;
    public static final String PREVIEW_MODE = "preview_mode";

    public static final String SHARE_BOTTOM_SHEET_DIALOG_TAG = "bottom_sheet_share";
    public static final String SUPPORT_BOTTOM_SHEET_DIALOG_TAG = "bottom_sheet_support_phone";

    public static final String WHATSAPP_PACKAGE_NAME = "com.whatsapp";
    public static final String GMAIL_PACKAGE_NAME = "com.google.android.gm";

    public static final String TEST_CREATED_BY_AHAGURU = "ahaguru";
    public static final String TEST_CREATED_BY_ELF = "elf english";
    public static final String TEACHER_STATUS_DELETED = "D";
}
