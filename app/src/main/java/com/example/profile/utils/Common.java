package com.example.profile.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import com.example.profile.R;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Common {

    public static void putDebugLog(String message) {
        if (Constants.DEVELOPMENT_MODE) {
            Log.d(Constants.DEBUG_TAG, message);
        }
    }

    public static void putErrorLog(String message) {
        if (Constants.DEVELOPMENT_MODE) {
            Log.e(Constants.DEBUG_TAG, message);
        }
    }

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void hideKeyboard(View view, Context context) {
        InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showKeyboardToGivenEdittext(EditText edittext, Context context){
        edittext.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittext, InputMethodManager.SHOW_IMPLICIT);
    }

    public static boolean isGivenStringNotNullAndNotEmpty(String str) {
        return str != null && !str.isEmpty() && !str.trim().isEmpty();
    }

    public static boolean isGivenStringNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    public static boolean isGivenMobileNumberValid(String text) {
        return text != null && !text.isEmpty() && text.matches("^\\d+$") && (text.length() >= Constants.MOBILE_NUMBER_MIN_LENGTH && text.length() <= Constants.MOBILE_NUMBER_MAX_LENGTH);
    }

    public static boolean isEmailValid(String email) {
        if (email == null)
            return false;
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String getMobileNumber(String contactNumber) {
        String[] mobileData = contactNumber.split(Constants.MOBILE_NO_DELIMITER);
        if (mobileData.length == 2 && Common.isGivenStringNotNullAndNotEmpty(mobileData[1])) {
            return mobileData[1];
        } else {
            return "";
        }
    }

    public static int getMobileCountryCode(String contactNumber) {
        String[] mobileData = contactNumber.split(Constants.MOBILE_NO_DELIMITER);
        if (mobileData.length > 0 && Common.isGivenStringNotNullAndNotEmpty(mobileData[0])) {
            return Integer.parseInt(mobileData[0]);
        } else {
            return Constants.DEFAULT_COUNTRY_CODE;
        }
    }

    public static boolean isObjectNotNullOrEmpty(Object object) {
        if (null != object) {
            return (!object.toString().equals(""));
        } else {
            return false;
        }
    }

    public static void copyToClipboard(String content, Context context) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Classroom code", content);
        clipboard.setPrimaryClip(clip);
       // Common.showToast(context, context.getResources().getString(R.string.copied_classroom_code));
    }

    public static void shareContent(String packageName, String content, String title, Context context) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, content);
        if(Common.isObjectNotNullOrEmpty(packageName)){
            shareIntent.setPackage(packageName);
        }
        context.startActivity(Intent.createChooser(shareIntent, (isObjectNotNullOrEmpty(title))?title:content));
    }

    public static void openSupportedApp(Uri uri, Context context, String title){
        Intent emailIntent = new Intent(Intent.ACTION_VIEW);
       emailIntent.setData(uri);
        context.startActivity(Intent.createChooser(emailIntent,title));
    }

    public static void tokenMismatchHandle(Activity activity) {
      //  signOut(activity);
    }



    public static String getSecondsInTimerFormat(long remainingMilliseconds) {
        return String.format(Locale.US, "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(remainingMilliseconds),
                TimeUnit.MILLISECONDS.toMinutes(remainingMilliseconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(remainingMilliseconds)),
                TimeUnit.MILLISECONDS.toSeconds(remainingMilliseconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingMilliseconds)));
    }

    @NotNull
    public static String getClassRoomNameWithClassPrefix(int standard, String name) {
        return "Class " + getClassRoomName(standard, name);
    }

    @NotNull
    public static String getClassRoomName(int standard, String name) {
        return getStandardStr(standard) + " " + name;
    }

    public static String getStandardStr(int standardIndex) {
        try {
            return Constants.standardList.get(standardIndex);
        } catch (IndexOutOfBoundsException e) {
            return "Old"; //todo - remove this for production release
        }
    }

    /**
     * Replaces all the chars to empty except A-Z a-z space and 0-9
     *
     * @param inputStr           given char sequence
     * @param parentNameEditText need to set filtered inputStr to the this edit text
     */
    public static void editBoxTextFilter(CharSequence inputStr, EditText parentNameEditText, String allowedTextRegex) {
        String newStr = inputStr.toString();
        newStr = newStr.replaceAll(allowedTextRegex, "");
        if (!inputStr.toString().equals(newStr)) {
            parentNameEditText.setText(newStr);
            parentNameEditText.setSelection(newStr.length());
        }
    }

    public static List<String> getFilteredClassList(String classIndicesStr) {
        String[] classIndices = classIndicesStr.split(",");
        List<String> filteredClassList = new ArrayList<>();
        for (String index : classIndices) {
            filteredClassList.add(Constants.standardList.get(Integer.parseInt(index)));
        }
        return filteredClassList;
    }



    public static String getCurrentTimeStamp(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getDefault());
            String currentTimeStamp = dateFormat.format(new Date());
            return currentTimeStamp;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getFirstLetterForProfileIcon(String name){
        if(Common.isGivenStringNotNullAndNotEmpty(name)){
            return name.substring(0, 1).toUpperCase();
        }
        return "";
    }
    public static  int getGivenStandardIndex(String standardStr){
        return Constants.standardList.indexOf(standardStr);
    }




    public static Drawable getIconFromPackageName(String packageName, Context context)
    {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(packageName, 0);
            Context otherAppCtx = context.createPackageContext(packageName, Context.CONTEXT_IGNORE_SECURITY);
            int[] displayMetrics = {DisplayMetrics.DENSITY_XHIGH, DisplayMetrics.DENSITY_HIGH, DisplayMetrics.DENSITY_TV};
            for (int displayMetric : displayMetrics) {
                try {
                    Drawable d = otherAppCtx.getResources().getDrawableForDensity(pi.applicationInfo.icon, displayMetric);
                    if (d != null) {
                        return d;
                    }
                }
                catch (Resources.NotFoundException e) {
                      Common.putErrorLog( "NameNotFound for" + packageName + " @ density: " + displayMetric);
                }
            }
        }
        catch (Exception e) {
            if(Constants.DEVELOPMENT_MODE)
            e.printStackTrace();
        }
        ApplicationInfo appInfo;
        try {
            appInfo = pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
        }
        catch (PackageManager.NameNotFoundException e) {
            if(Constants.DEVELOPMENT_MODE)
            e.printStackTrace();
            return null;
        }
        return appInfo.loadIcon(pm);
    }
}
